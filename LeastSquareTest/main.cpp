#include "MyPointCloud.h"
#include "MyViewer.h"
#include "My3DICPSolver.h"

// --------------------------
// -----Global Variables-----
// --------------------------
// (these can be given in input)
double max_distance = .05;
int max_ICP_iters = 100;
double min_chisquare_error = .000175;
bool downsample_pointclouds = true;
// (these cannot be given in input)
// viewer params
int viewer_rate = 100;
int rendering_point_size = 5;
int viewer_bground_r = 0; int viewer_bground_g = 0; int viewer_bground_b = 0;
int scene_color_r = 0; int scene_color_g = 0; int scene_color_b = 255;
int obj_color_r = 0; int obj_color_g = 255; int obj_color_b = 0;

// --------------
// -----Help-----
// --------------
std::string ExtractFilename( const std::string& path )
{
  return path.substr( path.find_last_of( '/' ) +1 );
}
void printUsage (const char* progPath)
{
  std::string progName = ExtractFilename(std::string(progPath));
  std::cout << "\n\nUsage: ./"<<progName<<" -s <scene.pcd> -o <object.pcd> [Options]\n"
            << "Example: ./"<<progName<<" -s scene.pcd -o object.pcd -md 0.05 -mi 100 -mc 0.125\n\n"
            << "Options:\n"
            << "-------------------------------------------\n"
            << "-s <string>   path of the scene.pcd file\n"
            << "-o <string>   path of the object.pcd file\n"
            << "-nd           if given, downsampling of the point clouds is disabled\n"
            << "-md <float>   max distance for accepting correspondence [default .05]\n"
            << "-mi <int>     max least square iterations [default 100]\n"
            << "-mc <float>   min chi square error accepted as termination condition of least square [default .125]\n"
            << "-h            this help\n"
            << "\n\n";
}

// --------------
// -----Main-----
// --------------
int main (int argc, char** argv)
{  
  if (argc < 5) { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
    printUsage (argv[0]);
    return 0;
  }
  else {
    //reading global params
    if (pcl::console::find_argument (argc, argv, "-h") >= 0) {
      printUsage (argv[0]);
      return 0;
    }
    if (pcl::console::find_argument (argc, argv, "-nd") >= 0) {
      downsample_pointclouds = false;
      cout << "Setting downsampling to false\n";
    }
    if (pcl::console::parse (argc, argv, "-md", max_distance) >= 0)
      cout << "Setting max_distance to "<<max_distance<<"\n";
    if (pcl::console::parse (argc, argv, "-mi", max_ICP_iters) >= 0)
      cout << "Setting max_ICP_iters to "<<max_ICP_iters<<"\n";
    if (pcl::console::parse (argc, argv, "-mc", min_chisquare_error) >= 0)
      cout << "Setting min_chisquare_error to "<<min_chisquare_error<<"\n\n";

    //reading the point clouds
    std::string scene_filename;
    std::string object_filename;
    if (pcl::console::parse (argc, argv, "-s", scene_filename) >= 0 && pcl::console::parse (argc, argv, "-o", object_filename) >= 0){
      cout << "Reading scene and object point clouds from " << scene_filename << " and " << object_filename << std::endl;
    }
    MyPointCloud sceneCloud;
    MyPointCloud objectCloud;
    if (sceneCloud.loadPointCloud(scene_filename, "scene_cloud", scene_color_r, scene_color_g, scene_color_b, downsample_pointclouds) && objectCloud.loadPointCloud(object_filename, "obj_cloud", obj_color_r, obj_color_g, obj_color_b, downsample_pointclouds))
    {
      std::cout << "Loaded " << sceneCloud.cloud()->width * sceneCloud.cloud()->height << " data points from " << sceneCloud.filename() << std::endl;
      std::cout << "Loaded " << objectCloud.cloud()->width * objectCloud.cloud()->height << " data points from " << objectCloud.filename() << std::endl;
    }
    else
    {
      std::cout << "Couldn't load the clouds." << std::endl;
    }

    //init the viewer
    MyViewer viewer(viewer_rate, rendering_point_size, viewer_bground_r, viewer_bground_g, viewer_bground_b);
    viewer.addCloud(sceneCloud);
    viewer.addCloud(objectCloud);

    //perform ICP
    std::cout << "Starting Least Square ..." << std::endl;
    int ICP_iters_ctr = 0;
    double chi_stats[max_ICP_iters];
    bool continue_running_ICP = true;
    Eigen::Affine3f init_guess;
    init_guess.setIdentity();
    My3DICPSolver solver(objectCloud, sceneCloud, init_guess);
    while (!viewer.wasStopped())
    {
      if(ICP_iters_ctr < max_ICP_iters && continue_running_ICP){
        std::vector<Correspondence> corrs;
        //the cloud of the object is the source_cloud in the correspondence finder!!!
        int corr_num = objectCloud.findCorrespondences(sceneCloud, max_distance, corrs);
        //std::cout << "Num corr: " << corr_num << std::endl;
        //draw correspondences
        //viewer.drawCorrespondences(objectCloud.cloud_id(), sceneCloud.cloud_id(), corrs);
        //one round
        solver.setCorrespondences(corrs);
        chi_stats[ICP_iters_ctr] = solver.doOneRoundICP();
        std::cout << "Chi square error: " << chi_stats[ICP_iters_ctr] << std::endl;
        //Eigen::Affine3f tf = solver.getTransform();
        //objectCloud.transformCloud(tf.matrix());
        objectCloud = solver.sourceCloud();
        viewer.updateCloud(objectCloud);
        if(chi_stats[ICP_iters_ctr] <= min_chisquare_error) {
          continue_running_ICP = false;
          std::cout << "Least Square converged within " << ICP_iters_ctr << " iterations" << std::endl;
          std::cout << "Last transform found:\n" << solver.getTransform().matrix() << std::endl;
        }
        else {
          ICP_iters_ctr++;
        }
      }
      if(ICP_iters_ctr == max_ICP_iters && continue_running_ICP){
        continue_running_ICP = false;
        std::cout << "Least square terminated. Max iterations reached: " << max_ICP_iters << std::endl;
      }
      viewer.show();
      //viewer->removePointCloud(sceneCloud.cloud_id());
      //viewer->addPointCloud<pcl::PointXYZ> (sceneCloud.cloud(), *sceneCloud.color_handler(), sceneCloud.cloud_id());
    }

    return (0);
  
  }
}
