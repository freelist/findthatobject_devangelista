#ifndef MYFEATUREEXTRACTOR_H
#define MYFEATUREEXTRACTOR_H

#include "MyPointCloud.h"

#include <pcl/range_image/range_image_planar.h>
#include <pcl/features/range_image_border_extractor.h>
#include <pcl/keypoints/narf_keypoint.h>
#include <pcl/features/narf_descriptor.h>

class MyFeatureExtractor{
public:
  MyFeatureExtractor(MyPointCloud cloud);
  //accessors
  MyPointCloud cloud() { return _cloud; }
  pcl::PointCloud<int>::Ptr keypoints() { return _keypoints; }
  pcl::PointCloud<pcl::PointXYZ>::Ptr keypoints_cloud() { return _keypoints_cloud; }
  pcl::PointCloud<pcl::PointXYZ>::Ptr descriptors_cloud() { return _descriptors_cloud; }
  std::vector<int> keypoints_std_vector() { return _keypoints_std_vector; }
  pcl::PointCloud<pcl::Narf36>::Ptr descriptors() { return _descriptors; }
  pcl::RangeImagePlanar::Ptr rangeImagePlanar() { return _rangeImagePlanar; }
private:
  MyPointCloud _cloud;
  pcl::PointCloud<int>::Ptr _keypoints;
  pcl::PointCloud<pcl::PointXYZ>::Ptr _keypoints_cloud;
  std::vector<int> _keypoints_std_vector;
  pcl::PointCloud<pcl::Narf36>::Ptr _descriptors;
  pcl::PointCloud<pcl::PointXYZ>::Ptr _descriptors_cloud;
  pcl::RangeImagePlanar::Ptr _rangeImagePlanar;
  pcl::RangeImageBorderExtractor::Ptr _borderExtractor;
  pcl::NarfKeypoint::Ptr _detector;
  //private methods
  void convertCloudToRangeImage();
  void extractKeypoints();
  void computeNarfFeatures();
};

#endif