#include "My3DICPSolver.h"

//rotation matrices
Eigen::Matrix3f Rx(float rot_x){
  float c=cos(rot_x);
  float s=sin(rot_x);
  Eigen::Matrix3f R;
  R << 1,  0, 0,
      0,  c,  -s,
      0,  s,  c;
  return R;
}

Eigen::Matrix3f Ry(float rot_y){
  float c=cos(rot_y);
  float s=sin(rot_y);
  Eigen::Matrix3f R;
  R << c,  0,  s,
      0 , 1,  0,
      -s,  0, c;
  return R;
}

Eigen::Matrix3f Rz(float rot_z){
  float c=cos(rot_z);
  float s=sin(rot_z);
  Eigen::Matrix3f R;
  R << c,  -s,  0,
      s,  c,  0,
      0,  0,  1;
  return R;
}

//v2t
Eigen::Isometry3f v2tEuler(const Eigen::Matrix<float, 6, 1>& v){
  Eigen::Isometry3f T;
  T.linear()=Rx(v[3])*Ry(v[4])*Rz(v[5]);
  T.translation()=v.head<3>();
  return T;
}

//skew decomposition
Eigen::Matrix3f skew(pcl::PointXYZ v){
  Eigen::Matrix3f S;
  S << 0, -v.z, v.y,
      v.z, 0, -v.x,
      -v.y, v.x, 0;
  return S;
}

Eigen::Vector3f pointsDifference(pcl::PointXYZ p1, pcl::PointXYZ p2){
  return Eigen::Vector3f(p1.x-p2.x, p1.y-p2.y, p1.z-p2.z);
}

double getVectorNorm(Eigen::Vector3f vec){
  return (double)sqrt(pow(vec[0],2)+pow(vec[1],2)+pow(vec[2],2));
}

My3DICPSolver::My3DICPSolver(MyPointCloud source_cloud, MyPointCloud target_cloud, Eigen::Affine3f transform) {
  _source_cloud = source_cloud;
  _target_cloud = target_cloud;
  _transform = transform;
}

ErrorAndJacobian My3DICPSolver::getErrorAndJacobian(pcl::PointXYZ p, pcl::PointXYZ z) {
  //compute the prediction
  ErrorAndJacobian result;
  pcl::PointXYZ z_hat = pcl::transformPoint(p, _transform);
  result.error = pointsDifference(z_hat, z);
  //std::cout << "Error: " << result.error.transpose() << std::endl;
  result.jacobian.setZero();
  result.jacobian.topLeftCorner(3,3).setIdentity();
  pcl::PointXYZ minus_z_hat(-z_hat.x, -z_hat.y, -z_hat.z);
  result.jacobian.topRightCorner(3,3) = skew(minus_z_hat);
  return result;
}

double My3DICPSolver::doOneRoundICP(){
  Eigen::Matrix<float, 6, 6> H;
  Eigen::Matrix<float, 6, 1> b;
  //set H and b to zero
  H.setZero();
  b.setZero();
  //initialize error to zero and run one round of ICP
  double chi_error = 0.0;
  int corr_size = _correspondences.size();
  for (int corr_idx=0; corr_idx<corr_size; corr_idx++){
    pcl::PointXYZ p = _source_cloud.cloud()->points[_correspondences[corr_idx].source_pt_idx];
    pcl::PointXYZ z = _target_cloud.cloud()->points[_correspondences[corr_idx].target_pt_idx];
    ErrorAndJacobian e_and_j = getErrorAndJacobian(p, z);
    //update H and b
    H += e_and_j.jacobian.transpose()*e_and_j.jacobian;
    b += e_and_j.jacobian.transpose()*e_and_j.error;
    //get error norm squared
    chi_error += e_and_j.error.squaredNorm();
  }
  //compute a solution
  Eigen::Matrix<float, 6, 1> dx = H.ldlt().solve(-b);
  _transform = v2tEuler(dx) * _transform;
  _source_cloud.transformCloud(_transform.matrix());
  return chi_error/corr_size;
}