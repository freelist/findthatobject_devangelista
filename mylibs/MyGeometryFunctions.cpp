#include "MyGeometryFunctions.h"

//createPlane
void computePlaneCenter(pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloudCenter){
  Eigen::Vector4f center_of_mass;
  pcl::compute3DCentroid(*inputCloud, center_of_mass);
  inputCloudCenter->points.clear();
  inputCloudCenter->points.push_back(*(new pcl::PointXYZ(center_of_mass[0], center_of_mass[1], center_of_mass[2])));
}

void createPlane(pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloudCenter, int plane_w, int plane_h, float plane_res){
  inputCloud->width = (unsigned int)round((float)plane_w / plane_res);
  inputCloud->height = (unsigned int)round((float)plane_h / plane_res);
  inputCloud->points.resize((inputCloud->width * inputCloud->height));
  // Generate the data
  for(int i=0; i<inputCloud->width; i++){
    for(int j=0; j<inputCloud->height; j++){
      inputCloud->points[i*inputCloud->width+j].x = (-plane_w/2 + i) * plane_res;
      inputCloud->points[i*inputCloud->width+j].y = (-plane_h/2 + j) * plane_res;
      inputCloud->points[i*inputCloud->width+j].z = 1.0;
    }
  }
  computePlaneCenter(inputCloud, inputCloudCenter);
  //std::cout << "PlaneCloud center of mass: " << center_of_mass << std::endl;
}

bool findElement(int vec[], int vec_size, int elem){
  for(int i=0; i<vec_size; i++){
    if(vec[i] == elem)
      return true;
  }
  return false;
}

//sample random points on plane
std::vector<pcl::PointXYZ> sampleRandomPointsOnPlane(pcl::PointCloud<pcl::PointXYZ>::Ptr planeCloud, int guess_num){
  std::vector<pcl::PointXYZ> random_guess_vector;
  int planeCloudSize = planeCloud->points.size();
  int indices[guess_num];
  int ctr = 0;
  for (int i=0; i<guess_num; i++) {
    indices[i] = -1;
  }
  while(ctr<guess_num) {
    int random_idx = (rand() % (planeCloudSize + 1));
    if(!findElement(indices, guess_num, random_idx)) {
      random_guess_vector.push_back(planeCloud->points[random_idx]);
      //std::cout << "InitialGuess[" << ctr << "]: " << random_guess_vector[ctr].getVector3fMap().transpose() << std::endl;
      ctr++;
    }
  }
  return random_guess_vector;
}

//geometry helpers fz
//rotation matrices
Eigen::Matrix4f R4x(float rot_x){
  float c=cos(rot_x);
  float s=sin(rot_x);
  Eigen::Matrix4f R;
  R << 1,  0,  0, 0,
      0,  c, -s, 0,
      0,  s,  c, 0,
      0,  0,  0, 1;
  return R;
}

Eigen::Matrix4f R4y(float rot_y){
  float c=cos(rot_y);
  float s=sin(rot_y);
  Eigen::Matrix4f R;
  R << c, 0, s, 0,
      0, 1, 0, 0,
      -s, 0, c, 0,
      0, 0, 0, 1;
  return R;
}

Eigen::Matrix4f R4z(float rot_z){
  float c=cos(rot_z);
  float s=sin(rot_z);
  Eigen::Matrix4f R;
  R << c, -s,  0, 0,
      s,  c,  0, 0,
      0,  0,  1, 0,
      0,  0,  0, 1;
  return R;
}