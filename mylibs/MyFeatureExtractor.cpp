#include "MyFeatureExtractor.h"

MyFeatureExtractor::MyFeatureExtractor(MyPointCloud cloud) {
  _cloud = cloud;
  _keypoints = boost::shared_ptr<pcl::PointCloud<int> >( new pcl::PointCloud<int>() );
  _keypoints_cloud = boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> >( new pcl::PointCloud<pcl::PointXYZ>() );
  _descriptors = boost::shared_ptr<pcl::PointCloud<pcl::Narf36> >( new pcl::PointCloud<pcl::Narf36>() );
  _descriptors_cloud = boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> >( new pcl::PointCloud<pcl::PointXYZ>() );
  _rangeImagePlanar = boost::shared_ptr<pcl::RangeImagePlanar>( new pcl::RangeImagePlanar() );
  _borderExtractor = boost::shared_ptr<pcl::RangeImageBorderExtractor>( new pcl::RangeImageBorderExtractor() );
  //create range image from cloud
  convertCloudToRangeImage();
  //extract the keypoints
  extractKeypoints();
  //compute narf features
  computeNarfFeatures();
}

void MyFeatureExtractor::convertCloudToRangeImage() {
  // Convert the cloud to range image.
  int imageSizeX = 640, imageSizeY = 480;
  float centerX = (640.0f / 2.0f), centerY = (480.0f / 2.0f);
  float focalLengthX = 525.0f;
  float focalLengthY = focalLengthX;
  Eigen::Affine3f sensorPose = Eigen::Affine3f(Eigen::Translation3f(_cloud.cloud_original()->sensor_origin_[0],
                                                                    _cloud.cloud_original()->sensor_origin_[1],
                                                                    _cloud.cloud_original()->sensor_origin_[2])) *
                               Eigen::Affine3f(_cloud.cloud_original()->sensor_orientation_);
  float noiseLevel = 0.0f, minimumRange = 0.0f;
  _rangeImagePlanar->createFromPointCloudWithFixedSize(*_cloud.cloud_original(), imageSizeX, imageSizeY,
                                               centerX, centerY, focalLengthX, focalLengthY,
                                               sensorPose, pcl::RangeImage::CAMERA_FRAME,
                                               noiseLevel, minimumRange);
}

void MyFeatureExtractor::extractKeypoints() {
  _detector = boost::shared_ptr<pcl::NarfKeypoint>( new pcl::NarfKeypoint(&(*_borderExtractor)) );
  _detector->setRangeImage(&(*_rangeImagePlanar));
  _detector->getParameters().support_size = 0.2f;
  _detector->compute(*_keypoints);
  //put keypoints in pointcloud - for visualization
  _keypoints_cloud->points.resize (_keypoints->points.size());
  for (size_t i=0; i<_keypoints->points.size (); ++i)
    _keypoints_cloud->points[i].getVector3fMap () = _rangeImagePlanar->points[_keypoints->points[i]].getVector3fMap();
}

void MyFeatureExtractor::computeNarfFeatures() {
  _keypoints_std_vector.resize(_keypoints->points.size());
  for (unsigned int i = 0; i < _keypoints->size(); ++i)
    _keypoints_std_vector[i] = _keypoints->points[i];
  pcl::NarfDescriptor narf(&(*_rangeImagePlanar), &_keypoints_std_vector);
  narf.getParameters().support_size = 0.2f;
  narf.getParameters().rotation_invariant = true;
  narf.compute(*_descriptors);
  //put descriptors in pointcloud - for visualization
  _descriptors_cloud->points.resize (_descriptors->points.size());
  for (size_t i=0; i<_descriptors->points.size (); ++i){
    _descriptors_cloud->points[i].x = _descriptors->points[i].x;
    _descriptors_cloud->points[i].y = _descriptors->points[i].y;
    _descriptors_cloud->points[i].z = _descriptors->points[i].z;
  }
}