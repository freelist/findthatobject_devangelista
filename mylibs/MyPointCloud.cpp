#include "MyPointCloud.h"

MyPointCloud::MyPointCloud()
{ 
  _cloud = boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> >( new pcl::PointCloud<pcl::PointXYZ>() );
  _cloud_original = boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> >( new pcl::PointCloud<pcl::PointXYZ>() );
  //init downsampling filter
  _pointcloud_filter.setInputCloud(_cloud);
  _pointcloud_filter.setLeafSize(0.02f, 0.02f, 0.02f);
  _target_cloud_KDTree_set = false;
}

MyPointCloud::MyPointCloud(std::string filename, std::string cloud_id, int r, int g, int b, bool downsample)
{
  _cloud = boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> >( new pcl::PointCloud<pcl::PointXYZ>() );
  _cloud_original = boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> >( new pcl::PointCloud<pcl::PointXYZ>() );
  _cloud_id = cloud_id;
  _filename = filename;
  //loading the pointcloud
  std::cout << "Loading data from " << filename <<" file ..."<< std::endl;
  if (pcl::io::loadPCDFile<pcl::PointXYZ> (filename, *_cloud) == -1) //* load the file
  {
    PCL_ERROR ("Couldn't read the file \n");
  }

  //save a copy of the original cloud
  _cloud_original = _cloud->makeShared();
  
  //init downsampling filter
  _pointcloud_filter.setInputCloud(_cloud);
  _pointcloud_filter.setLeafSize(0.01f, 0.01f, 0.01f);
  if(downsample)
  {
    downsamplePointCloud();
    std::cout << "Point cloud filtered "<< std::endl;
    _use_downsampled = downsample;
  }

  //get point cloud XdVector
  _cloud_xd_vector = getPointsXdVector();

  //set color_handler
  _r = r; _g = g; _b = b;
  _color_handler = boost::shared_ptr<pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> >( new pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>(_cloud, _r, _g, _b) );

  _target_cloud_KDTree_set = false;
}

bool MyPointCloud::loadPointCloud(std::string filename, std::string cloud_id, int r, int g, int b, bool downsample)
{
  _cloud_id = cloud_id;
  _filename = filename;
  //loading the pointcloud
  //std::cout << "Loading data from " << filename <<" file ..."<< std::endl;
  if (pcl::io::loadPCDFile<pcl::PointXYZ> (filename, *_cloud) == -1) //* load the file
  {
    PCL_ERROR ("Couldn't read the file \n");
    return false;
  }

  //save a copy of the original cloud
  _cloud_original = _cloud->makeShared();

  if(downsample)
  {
    downsamplePointCloud();
    //std::cout << "Point cloud filtered "<< std::endl;
    _use_downsampled = downsample;
  }

  //get point cloud XdVector
  _cloud_xd_vector = getPointsXdVector();

  //set color_handler
  _r = r; _g = g; _b = b;
  _color_handler = boost::shared_ptr<pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> >( new pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>(_cloud, _r, _g, _b) );

  _target_cloud_KDTree_set = false;

  return true;
}

bool MyPointCloud::loadPointCloud(MyPointCloud inputCloud)
{
  _cloud_id = inputCloud.cloud_id();
  _filename = inputCloud.filename();
  //loading the pointcloud
  //std::cout << "Loading data from " << filename <<" file ..."<< std::endl;
  _cloud = inputCloud.cloud()->makeShared();

  //save a copy of the original cloud
  _cloud_original = inputCloud.cloud_original()->makeShared();

  //get point cloud XdVector
  _cloud_xd_vector = inputCloud.cloud_xd_vector();

  //set color_handler
  _r = inputCloud.r(); _g = inputCloud.g(); _b = inputCloud.b();
  _color_handler = inputCloud.color_handler();

  _target_cloud_KDTree_set = false;

  return true;
}

void MyPointCloud::downsamplePointCloud()
{
  _pointcloud_filter.filter(*_cloud);
}

void MyPointCloud::transformCloud(Eigen::Matrix4f transform)
{
  pcl::transformPointCloud(*_cloud, *_cloud, transform);
  _cloud_xd_vector = getPointsXdVector();
}

int MyPointCloud::findPointIdx(VectorXd point) {
  int idx = -1;
  int cloud_size = _cloud->height * _cloud->width;
  for (int i=0; i<cloud_size; i++){
    if(_cloud->points[i].x == point(0) &&
       _cloud->points[i].y == point(1) &&
       _cloud->points[i].z == point(2))
      return i;
  }
  return idx;
}

void MyPointCloud::setTargetCloudKDTree(MyPointCloud target_cloud, double max_distance) {
  _target_cloud_KDTree = target_cloud.buildKdTree(max_distance);
  _target_cloud_KDTree_set = true;
}

int MyPointCloud::findCorrespondences(MyPointCloud target_cloud, double max_distance, std::vector< Correspondence >& correspondences)
{
  int corrs_ctr = 0;
  BaseTreeNode* kd_tree_target_cloud_root;
  if (!_target_cloud_KDTree_set) {
    this->setTargetCloudKDTree(target_cloud, max_distance);
  }
  kd_tree_target_cloud_root = _target_cloud_KDTree;
  double mean = 0;
  for (int i=0; i<_cloud_xd_vector.size(); i++){
    VectorXd answer;
    double approx_distance=kd_tree_target_cloud_root->findNeighbor(answer, _cloud_xd_vector[i], max_distance);
    int target_cloud_point_idx = target_cloud.findPointIdx(answer);
    if (target_cloud_point_idx>=0 && approx_distance>=0) {
      mean += abs(approx_distance);
      Correspondence corr;
      corr.source_pt_idx = i;
      corr.target_pt_idx = target_cloud_point_idx;
      correspondences.push_back(corr);
      corrs_ctr++;
    }
  }
  //std::cout<<"Approximate distance mean: "<<mean/(corrs_ctr)<<std::endl;
  return corrs_ctr;
}

double MyPointCloud::computeOverallErrorFromCloud(MyPointCloud queryCloud) {
  int distance = 100000;
  BaseTreeNode* kd_tree_target_cloud_root = queryCloud.buildKdTree(distance);
  double error = 0;
  int cloud_xd_vector_size = _cloud_xd_vector.size();
  for (int i=0; i<cloud_xd_vector_size; i++){
    VectorXd answer;
    double approx_distance=kd_tree_target_cloud_root->findNeighbor(answer, _cloud_xd_vector[i], distance);
    int target_cloud_point_idx = queryCloud.findPointIdx(answer);
    if (target_cloud_point_idx>=0 && approx_distance>=0) {
      error += abs(approx_distance);
    }
  }
  //std::cout<<"Approximate distance mean: "<<mean/(corrs_ctr)<<std::endl;
  return error/cloud_xd_vector_size;
}

VectorXdVector MyPointCloud::getPointsXdVector() {
  int points_size = _cloud->height * _cloud->width;
  VectorXdVector points(points_size);
  for (int i=0; i<points_size; i++) {
    Vector3d point(_cloud->points[i].x, _cloud->points[i].y, _cloud->points[i].z);
    points[i] = point;
  }
  return points;
}

BaseTreeNode* MyPointCloud::buildKdTree(double leaf_range)
{
  //VectorXdVector points = getPointsXdVector();
  BaseTreeNode* root = buildTree(_cloud_xd_vector, leaf_range);
  return root;
}

pcl::PointXYZ MyPointCloud::getCloudCenterOfMass() {
  Eigen::Vector4f center_of_mass;
  pcl::compute3DCentroid(*_cloud_original, center_of_mass);
  pcl::PointXYZ inputCloudCenter;
  inputCloudCenter = (*(new pcl::PointXYZ(center_of_mass[0], center_of_mass[1], center_of_mass[2])));
  return inputCloudCenter;
}