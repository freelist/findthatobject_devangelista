#include "MyBorderExtractor.h"

MyBorderExtractor::MyBorderExtractor(MyPointCloud cloud, float noise_level, float min_range, int border_size, float angular_resolution) {
  _cloud = cloud;
  _range_image_ptr = boost::shared_ptr<pcl::RangeImage >( new pcl::RangeImage );
  _noise_level = noise_level;
  _min_range = min_range;
  _border_size = border_size;
  _angular_resolution = pcl::deg2rad (angular_resolution);
  _coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
  _scene_sensor_pose.setIdentity();
  _scene_sensor_pose = Eigen::Affine3f (Eigen::Translation3f (_cloud.cloud()->sensor_origin_[0],
                                                              _cloud.cloud()->sensor_origin_[1],
                                                              _cloud.cloud()->sensor_origin_[2])) *
                       Eigen::Affine3f (_cloud.cloud()->sensor_orientation_);
  //create range image
  createRangeImage();
  //extract borders
  extractBorders();
  //initialize border veil and shadow points pointers
  _border_points_ptr = boost::shared_ptr<pcl::PointCloud<pcl::PointWithRange> >( new pcl::PointCloud<pcl::PointWithRange>() );
  _veil_points_ptr = boost::shared_ptr<pcl::PointCloud<pcl::PointWithRange> >( new pcl::PointCloud<pcl::PointWithRange>() );
  _shadow_points_ptr = boost::shared_ptr<pcl::PointCloud<pcl::PointWithRange> >( new pcl::PointCloud<pcl::PointWithRange>() );
  //compute points for visualization
  computeBorderVeilShadowPoints();
  //initialize color handlers
  _border_points_color_handler = boost::shared_ptr<pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> >( new pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>(_border_points_ptr, 0, 255, 0) );
  _veil_points_color_handler = boost::shared_ptr<pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> >( new pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>(_veil_points_ptr, 255, 0, 0) );
  _shadow_points_color_handler = boost::shared_ptr<pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> >( new pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>(_shadow_points_ptr, 0, 255, 255) );
}

void MyBorderExtractor::createRangeImage() {

  _range_image_ptr->createFromPointCloud (*_cloud.cloud_original(), _angular_resolution, pcl::deg2rad (360.0f), pcl::deg2rad (180.0f),
                                          _scene_sensor_pose, _coordinate_frame, _noise_level, _min_range, _border_size);
  _range_image_ptr->integrateFarRanges(_far_ranges);
  _range_image_ptr->setUnseenToMaxRange ();
}

void MyBorderExtractor::extractBorders() {
  pcl::RangeImageBorderExtractor border_extractor (&(*_range_image_ptr));
  border_extractor.compute (_border_descriptions);
}

void MyBorderExtractor::computeBorderVeilShadowPoints() {
  for (int y=0; y< (int)_range_image_ptr->height; ++y)
  {
    for (int x=0; x< (int)_range_image_ptr->width; ++x)
    {
      if (_border_descriptions.points[y*_range_image_ptr->width + x].traits[pcl::BORDER_TRAIT__OBSTACLE_BORDER])
        _border_points_ptr->points.push_back (_range_image_ptr->points[y*_range_image_ptr->width + x]);
      if (_border_descriptions.points[y*_range_image_ptr->width + x].traits[pcl::BORDER_TRAIT__VEIL_POINT])
        _veil_points_ptr->points.push_back (_range_image_ptr->points[y*_range_image_ptr->width + x]);
      if (_border_descriptions.points[y*_range_image_ptr->width + x].traits[pcl::BORDER_TRAIT__SHADOW_BORDER])
        _shadow_points_ptr->points.push_back (_range_image_ptr->points[y*_range_image_ptr->width + x]);
    }
  }
}