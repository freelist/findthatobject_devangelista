#ifndef MYVIEWER_H
#define MYVIEWER_H

#include <algorithm>
#include "MyPointCloud.h"
#include <pcl/visualization/pcl_visualizer.h>

class MyViewer{
public:
  MyViewer();
  MyViewer(int rate=100, int rendering_point_size=1);
  MyViewer(int rate=100, int rendering_point_size=1, int br=0, int bg=0, int bb=0);
  void show();
  bool addCloud(MyPointCloud cloud);
  bool removeCloud(MyPointCloud cloud);
  bool updateCloud(MyPointCloud cloud);
  bool wasStopped(){return _viewer->wasStopped();}
  bool drawCorrespondences(std::string source_cloud_id, std::string target_cloud_id, std::vector<Correspondence > correspondence);
  void setViewerName(std::string name) { _viewer->setWindowName(name); }
private:
  pcl::visualization::PCLVisualizer::Ptr _viewer;
  std::vector<MyPointCloud > _clouds;
  int _rate;
  int _rendering_point_size;
  //methods
  void setBgColor(int r, int g, int b);
  int getPointCloudIndexById(std::string cloud_id);
};

#endif
