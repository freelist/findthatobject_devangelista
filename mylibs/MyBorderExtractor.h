#ifndef MYBORDEREXTRACTOR_H
#define MYBORDEREXTRACTOR_H

#include "MyPointCloud.h"

#include <pcl/range_image/range_image.h>
#include <pcl/features/range_image_border_extractor.h>

typedef pcl::PointXYZ PointType;

class MyBorderExtractor{
public:
  MyBorderExtractor(MyPointCloud cloud, float noise_level=0.0, float min_range=0.0f, int border_size=1, float angular_resolution=0.6f);
  //accessors
  MyPointCloud cloud(){ return _cloud; }
  boost::shared_ptr<pcl::RangeImage> range_image_ptr() { return _range_image_ptr; }
  pcl::PointCloud<pcl::BorderDescription> border_descriptions(){ return _border_descriptions; }
  pcl::PointCloud<pcl::PointWithRange>::Ptr border_points_ptr() { return _border_points_ptr; }
  pcl::PointCloud<pcl::PointWithRange>::Ptr veil_points_ptr() { return _veil_points_ptr; }
  pcl::PointCloud<pcl::PointWithRange>::Ptr shadow_points_ptr() { return _shadow_points_ptr; }
  pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>::Ptr border_points_color_handler() { return _border_points_color_handler; }
  pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>::Ptr veil_points_color_handler() { return _veil_points_color_handler; }
  pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>::Ptr shadow_points_color_handler() { return _shadow_points_color_handler; }
private:
  MyPointCloud _cloud;
  boost::shared_ptr<pcl::RangeImage> _range_image_ptr;
  pcl::PointCloud<pcl::BorderDescription> _border_descriptions;
  pcl::PointCloud<pcl::PointWithRange>::Ptr _border_points_ptr;
  pcl::PointCloud<pcl::PointWithRange>::Ptr _veil_points_ptr;
  pcl::PointCloud<pcl::PointWithRange>::Ptr _shadow_points_ptr;
  pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>::Ptr _border_points_color_handler;
  pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>::Ptr _veil_points_color_handler;
  pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange>::Ptr _shadow_points_color_handler;
  //borderextractor params
  pcl::RangeImage::CoordinateFrame _coordinate_frame;
  Eigen::Affine3f _scene_sensor_pose;
  pcl::PointCloud<pcl::PointWithViewpoint> _far_ranges;
  float _angular_resolution;
  float _noise_level;
  float _min_range;
  int _border_size;
  //methods
  void createRangeImage();
  void extractBorders();
  void computeBorderVeilShadowPoints();
};

#endif