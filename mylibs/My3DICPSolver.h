#ifndef MY3DICPSOLVER_H
#define MY3DICPSOLVER_H

#include "MyPointCloud.h"
#include <pcl/common/geometry.h>
#include <Eigen/Cholesky>

struct ErrorAndJacobian{
  Eigen::Vector3f error;
  Eigen::Matrix<float, 3, 6> jacobian;
};

class My3DICPSolver{
public:
  My3DICPSolver(){};
  My3DICPSolver(MyPointCloud source_cloud, MyPointCloud target_cloud, Eigen::Affine3f transform);
  //public methods
  double doOneRoundICP();
  void setCorrespondences(std::vector< Correspondence > correspondences) { _correspondences = correspondences; }
  //accessors
  MyPointCloud sourceCloud(){ return _source_cloud; }
  MyPointCloud targetCloud(){ return _target_cloud; }
  std::vector< Correspondence > correspondences() { return _correspondences; }
  Eigen::Affine3f getTransform(){ return _transform; }
private:
  MyPointCloud _source_cloud;
  MyPointCloud _target_cloud;
  std::vector<Correspondence> _correspondences;
  Eigen::Affine3f _transform;
  //private methods
  ErrorAndJacobian getErrorAndJacobian(pcl::PointXYZ p, pcl::PointXYZ z);
};

#endif