#ifndef MYPOINTCLOUD_H
#define MYPOINTCLOUD_H

#include <iostream>
#include <string.h>

#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/voxel_grid.h>

#include "MyKdTree.h"

struct Correspondence{
  int source_pt_idx;
  int target_pt_idx;
};

class MyPointCloud{
public:
  MyPointCloud();
  MyPointCloud(std::string filename, std::string cloud_id, int r=255, int g=0, int b=0, bool downsample=true);
  //accessors
  std::string cloud_id(){return _cloud_id;}
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(){return _cloud;}
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_original(){return _cloud_original;}
  VectorXdVector cloud_xd_vector(){return _cloud_xd_vector;}
  bool use_downsampled(){return _use_downsampled;};
  std::string filename(){return _filename;}
  pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>::Ptr color_handler(){return _color_handler;}
  int r(){return _r;}
  int g(){return _g;}
  int b(){return _b;}
  //public methods
  bool loadPointCloud(std::string filename, std::string cloud_id, int r=255, int g=0, int b=0, bool downsample=true);
  bool loadPointCloud(MyPointCloud inputCloud);
  void transformCloud(Eigen::Matrix4f transform);
  int findCorrespondences(MyPointCloud target_cloud, double max_distance, std::vector<Correspondence> & correspondences);
  BaseTreeNode* buildKdTree(double leaf_range);
  int findPointIdx(VectorXd point);
  pcl::PointXYZ getCloudCenterOfMass();
  double computeOverallErrorFromCloud(MyPointCloud queryCloud);
  void setTargetCloudKDTree(MyPointCloud target_cloud, double max_distance);

private:
  std::string _cloud_id;
  std::string _filename;
  pcl::PointCloud<pcl::PointXYZ>::Ptr _cloud;
  pcl::PointCloud<pcl::PointXYZ>::Ptr _cloud_original;
  VectorXdVector _cloud_xd_vector;
  pcl::VoxelGrid<pcl::PointXYZ > _pointcloud_filter;
  pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>::Ptr _color_handler;
  BaseTreeNode* _target_cloud_KDTree;
  bool _target_cloud_KDTree_set;
  bool _use_downsampled;
  int _r;
  int _g;
  int _b;
  //private methods
  void downsamplePointCloud();
  VectorXdVector getPointsXdVector();
};

#endif
