#include "MyViewer.h"

int findCloud(std::vector<MyPointCloud> vec, MyPointCloud cloud)
{
  for (int i=0; i<vec.size(); i++)
  {
    if (cloud.cloud_id().compare(vec[i].cloud_id()) == 0)
    {
      return i;
    }
  }
  return -1;
}

MyViewer::MyViewer()
{
  _viewer = boost::shared_ptr<pcl::visualization::PCLVisualizer >( new pcl::visualization::PCLVisualizer ("3D Viewer") );
  setBgColor(0, 0, 0);
  _rate = 100;
  _rendering_point_size = 1;
}

MyViewer::MyViewer(int rate, int rendering_point_size)
{
  _viewer = boost::shared_ptr<pcl::visualization::PCLVisualizer >( new pcl::visualization::PCLVisualizer ("3D Viewer") );
  setBgColor(0, 0, 0);
  _rate = rate;
  _rendering_point_size = rendering_point_size;
}

MyViewer::MyViewer(int rate, int rendering_point_size, int br, int bg, int bb)
{
  _viewer = boost::shared_ptr<pcl::visualization::PCLVisualizer >( new pcl::visualization::PCLVisualizer ("3D Viewer") );
  setBgColor(br, bg, bb);
  _rate = rate;
  _rendering_point_size = rendering_point_size;
}

void MyViewer::setBgColor(int r, int g, int b)
{
  _viewer->setBackgroundColor(r, g, b);
}

bool MyViewer::addCloud(MyPointCloud cloud)
{
  int cloud_idx = findCloud(_clouds, cloud);
  if ( cloud_idx == -1 )
  {
    _clouds.push_back(cloud);
    return true;
  }
  else
  {
   return false;
  }
}

bool MyViewer::removeCloud(MyPointCloud cloud)
{
  int cloud_idx = findCloud(_clouds, cloud);
  if ( cloud_idx > 0 )
  {
    _clouds.erase(_clouds.begin() + cloud_idx);
    return true;
  }
  else
  {
   return false;
  }
}

bool MyViewer::updateCloud(MyPointCloud cloud)
{
  int cloud_idx = findCloud(_clouds, cloud);
  if ( cloud_idx > 0 )
  {
    _clouds[cloud_idx] = cloud;
    return true;
  }
  else
  {
   return false;
  }
}

void MyViewer::show()
{
  _viewer->removeAllPointClouds();
  _viewer->removeAllShapes();
  for (int i=0; i<_clouds.size(); i++)
  {
    //_viewer->removePointCloud(_clouds[i].cloud_id());
    _viewer->addPointCloud<pcl::PointXYZ> (_clouds[i].cloud(), *_clouds[i].color_handler(), _clouds[i].cloud_id());
    _viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, _rendering_point_size, _clouds[i].cloud_id());
  }
  _viewer->spinOnce(_rate);
}

int MyViewer::getPointCloudIndexById(std::string cloud_id) {
  for(int i=0;i<_clouds.size(); i++){
    if(cloud_id.compare(_clouds[i].cloud_id()) == 0)
      return i;
  }
  return -1;
}

//source cloud must always be the object cloud
bool MyViewer::drawCorrespondences(std::string source_cloud_id, std::string target_cloud_id,
                                   std::vector<Correspondence> correspondence) {
  bool result = false;
  bool source_cloud_found = false;
  bool target_cloud_found = false;
  for(int i=0; i<_clouds.size(); i++){
    if(_clouds[i].cloud_id().compare(source_cloud_id)==0)
      source_cloud_found = true;
    if(_clouds[i].cloud_id().compare(target_cloud_id)==0)
      target_cloud_found = true;
  }
  if(source_cloud_found && target_cloud_found){
    result = true;
    int source_cloud_idx = getPointCloudIndexById(source_cloud_id);
    int target_cloud_idx = getPointCloudIndexById(target_cloud_id);
    if(source_cloud_idx>=0 && target_cloud_idx>=0) {
      for (int i = 0; i < correspondence.size(); i++) {
        std::stringstream sstmp;
        sstmp << "correspondence_line_" << correspondence[i].source_pt_idx << "_" << correspondence[i].target_pt_idx;
        std::string line_id = sstmp.str();
        _viewer->addLine<pcl::PointXYZ>(_clouds[source_cloud_idx].cloud()->points[correspondence[i].source_pt_idx],
                                        _clouds[target_cloud_idx].cloud()->points[correspondence[i].target_pt_idx],
                                        255, 0, 0,
                                        line_id);
      }
    }
  }
  return result;
}

