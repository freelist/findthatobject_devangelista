#ifndef MYGEOMETRYFUNCTIONS_H
#define MYGEOMETRYFUNCTIONS_H

#include "MyPointCloud.h"

//createPlane
void computePlaneCenter(pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloud,
                        pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloudCenter);

void createPlane(pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloud,
                 pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloudCenter,
                 int plane_w,
                 int plane_h,
                 float plane_res);
//sample points on plane
std::vector<pcl::PointXYZ> sampleRandomPointsOnPlane(pcl::PointCloud<pcl::PointXYZ>::Ptr planeCloud, int guess_num);

//rotation matrices
Eigen::Matrix4f R4x(float rot_x);

Eigen::Matrix4f R4y(float rot_y);

Eigen::Matrix4f R4z(float rot_z);

#endif