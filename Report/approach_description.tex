% APPROACH DESCRIPTION
As anticipated in the introduction, the main focus of my project was to build robust and efficient Iteratice Closest Point procedure in order to refine the position of the object within the scene starting from a quite good initial guess.\\
Finding the good initial guess is not so easy, especially in a three dimensional cartesian space. Most of the techniques rely on 3D feature extraction and matching, and in most of the cases, such approaches are not so effective and not so efficient. The approach taken in this project simplify the problem by letting the user choose the plane where \emph{n} random initial guesses are taken, then for each of them, ICP is performed till the overall error goes below a given threshold.\\

\subsection*{ICP for 3D points alignment}
\addcontentsline{toc}{subsection}{ICP for 3D points alignment}
My implementation of ICP is based on Least Square minimization of the error between a set of 3D points in the scene and in the object point clouds.\\
We first need to identify the state $X$:
\begin{align*}
X = [R|t] \in SE(3)
\end{align*}
The increment $\Delta X$:
\begin{align*}
\Delta X = (\Delta t \Delta \alpha)^T = (\Delta X \Delta Y \Delta Z \Delta \alpha_x \Delta \alpha_y \Delta \alpha_z)^T
\end{align*}
The box-plus operation:
\begin{align*}
X \boxplus \Delta X &= v2t(\Delta X) X \\
&= [R(\Delta \alpha)R|R(\Delta \alpha)t + \Delta t]
\end{align*}
The prediction function $h_i(X \boxplus \Delta X)$ is trivial, since we are dealing with points in 3D space and the state is a transform matrix:
\begin{align*}
h_i(X \boxplus \Delta X) = X p_i
\end{align*}
For the very same reason, the error function is simply a point to point difference:
\begin{align*}
z \in R^3
\end{align*}
Given all the previous equations, we can build the least square algorithm in the following way:
\begin{algorithm}[H]
 \ForAll{iteration}{
  H $\leftarrow$ 0\;
  b $\leftarrow$ 0\;
  \ForAll{correspondences}{
   $e_i \leftarrow (h_i - z_i)$\;
   $J_i \leftarrow \frac{\partial e(X + \Delta X)}{\partial \Delta X}\Bigr|_{\Delta X = 0} = [I \ [-p_i]_X]$\;
   $H \leftarrow H + J_i^T \Omega_i J_i$\;
   $b \leftarrow  b + J_i^T \Omega_i e_i$\;
  }
  $\Delta X \leftarrow solve(H \Delta X = -b)$\;
  $X \leftarrow X \boxplus \Delta X$\;
 }
\caption{Least Square ICP algorithm}
\end{algorithm}
The matrix $J$ is the so called Jacobian, the derivative of the error function with respect the perturbation $\Delta X$ applied to the state $X$.\\
The Jacobian for the ICP problem is a $3 by 6$ matrix where the first $3 by 3$ block is a \emph{Identity} and the last one is the \emph{skew simmetric of $-p_i$}.\\

\subsection*{Correspondence finding}
\addcontentsline{toc}{subsection}{Correspondence finding}
The approach given before is based on the computation of the error within a given set of 3D points in the object point cloud and their relative correspondences in the scene one.\\
One of the difficult and tricky part in that procedure, is finding exactly the correspondences between the two point clouds. As anticipated in the introduction, many techniques rely on feature extraction and matching, while many others on nearest neighbors finding. My approach is based on \emph{KD-tree}, a complex data structure, mainly thought for searching procedures, that basically partitions the query points according to their spatial distribution.\\
Such approach is computationally more efficient than any other method, e.g. distance map, especially if the tree is balanced, the complexity goes with $log(n)$, where $n$ is the number of points to split.\\

Building a KD-tree is relatively trivial, basically, at each time the set is split in two parts until the number of points in a leaf is smaller than a threshold. The most interesting part is in \emph{how to split the points}.\\
In Figure \ref{fig:split_kdtree} there is an idea of what is the approach in the splitting phase. In particular, if we consider that the n-dimensional points are normally distributed, we could:
\begin{itemize}
	\item Compute the \emph{Covariance} matrix of the distribution;
	\item then, chose a splitting (hyper) plane that passes through the mean and has a normal aligned with the longest axis of the covariance matrix.\\
\end{itemize}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.4\textwidth]{split_kdtree}
	\caption{Splitting points approach in KD-tree.}
	\centering
	\label{fig:split_kdtree}
\end{figure}
Given such splitting plane, it is trivial to check if one query point lies on one side of the plane or on the other, we can simply test if the scalar product of the normal to the plane and the vector of the difference between the point and the center of the distribution is less or more than zero, this will give the part of the plane where the point is:
\begin{align*}
n_i^T (p_q - \mu_i) > 0
\end{align*}