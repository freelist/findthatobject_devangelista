#include "MyPointCloud.h"
#include "MyGeometryFunctions.h"
#include "My3DICPSolver.h"

#include <time.h>

// --------------------------
// -----Global Variables-----
// --------------------------
// (these can be given in input)
double max_distance = .05;
int max_ICP_iters = 100;
double min_chisquare_error = .000175;
bool downsample_pointclouds = true;
bool debug_mode = false;
int initial_guess_number = 10;
// (these cannot be given in input)
// viewer params
int viewer_rate = 100;
int rendering_point_size = 1;
int rendering_plane_center_point_size = 7;
int viewer_bground_r = 0; int viewer_bground_g = 0; int viewer_bground_b = 0;
int scene_color_r = 0; int scene_color_g = 0; int scene_color_b = 255;
int obj_color_r = 0; int obj_color_g = 255; int obj_color_b = 0;
int plane_color_r = 255; int plane_color_g = 0; int plane_color_b = 0;
int plane_w = 1;
int plane_h = 1;
float plane_res = 0.01;
bool show_normals = false;

//plane data pointers and vars
pcl::PointCloud<pcl::PointXYZ>::Ptr planeCloud(new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr planeCenter(new pcl::PointCloud<pcl::PointXYZ>);
Eigen::Matrix4f transform_from_plane_center = Eigen::Matrix4f::Identity();
Eigen::Matrix4f transform_to_plane_center = Eigen::Matrix4f::Identity();
float rot_x = 0.0;
float rot_y = 0.0;
float rot_z = 0.0;
std::vector<pcl::PointXYZ> initial_guess_vector;

// --------------
// -----Help-----
// --------------
std::string ExtractFilename( const std::string& path )
{
  return path.substr( path.find_last_of( '/' ) +1 );
}
void printUsage (const char* progPath)
{
  std::string progName = ExtractFilename(std::string(progPath));
  std::cout << "\n\nUsage: ./"<<progName<<" -s <scene.pcd> -o <object.pcd> [Options]\n"
            << "Example: ./"<<progName<<" -s scene.pcd -o object.pcd -r 10 -md 0.05 -mi 100 -mc 0.000175\n\n"
            << "Options:\n"
            << "-------------------------------------------\n"
            << "-s <string>   path of the scene.pcd file\n"
            << "-o <string>   path of the object.pcd file\n"
            << "-nd           if given, downsampling of the point clouds is disabled\n"
            << "-r <int>      number of random initial guess on plane (default is 10)\n"
            << "-md <float>   max distance for accepting correspondence [default .05]\n"
            << "-mi <int>     max least square iterations [default 100]\n"
            << "-mc <float>   min chi square error accepted as termination condition of least square [default .125]\n"
            << "-v            if given, debug mode is enabled\n"
            << "-h            this help\n"
            << "\n\n";
}
void printPlaneSelectionUsage() {
  std::cout << "Up, Down, Left, Right   translate the center of the plane on XY plane\n"
            << "è, à                    translate the plane on Z axis\n"
            << "w, s                    rotate the plane on X axis\n"
            << "a, d                    rotate the plane on Y axis\n"
            << "ò, ù                    rotate the plane on Z axis\n\n";
}

//keyboardEvent Callback
void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event, void* viewer_void)
{
  float step = 0.05;
  float rad_step = 0.1;
  transform_from_plane_center.setIdentity();
  transform_from_plane_center (0,3) = -planeCenter->points[0].x;
  transform_from_plane_center (1,3) = -planeCenter->points[0].y;
  transform_from_plane_center (2,3) = -planeCenter->points[0].z;
  transform_to_plane_center.setIdentity();
  transform_to_plane_center (0,3) = planeCenter->points[0].x;
  transform_to_plane_center (1,3) = planeCenter->points[0].y;
  transform_to_plane_center (2,3) = planeCenter->points[0].z;
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *>(viewer_void);
  if (event.getKeySym() == "n" && event.keyDown()) {
    show_normals = !show_normals;
  }
  else if (event.getKeySym() == "Up" && event.keyDown()){
    Eigen::Matrix4f transform_up = Eigen::Matrix4f::Identity();
    transform_up (1,3) = -step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_up);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "Down" && event.keyDown()){
    Eigen::Matrix4f transform_down = Eigen::Matrix4f::Identity();
    transform_down (1,3) = step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_down);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "Left" && event.keyDown()){
    Eigen::Matrix4f transform_left = Eigen::Matrix4f::Identity();
    transform_left (0,3) = -step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_left);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "Right" && event.keyDown()){
    Eigen::Matrix4f transform_right = Eigen::Matrix4f::Identity();
    transform_right (0,3) = step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_right);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "egrave" && event.keyDown()){
    Eigen::Matrix4f transform_zoom_in = Eigen::Matrix4f::Identity();
    transform_zoom_in (2,3) = step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_zoom_in);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "agrave" && event.keyDown()){
    Eigen::Matrix4f transform_zoom_out = Eigen::Matrix4f::Identity();
    transform_zoom_out (2,3) = -step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_zoom_out);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "ograve" && event.keyDown()){
    float theta = -rad_step; // The angle of rotation in radians
    rot_z += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4z(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "ugrave" && event.keyDown()){
    float theta = rad_step; // The angle of rotation in radians
    rot_z += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4z(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "a" && event.keyDown()){
    float theta = rad_step; // The angle of rotation in radians
    rot_y += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4y(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "d" && event.keyDown()){
    float theta = -rad_step; // The angle of rotation in radians
    rot_y += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4y(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "w" && event.keyDown()){
    float theta = -rad_step; // The angle of rotation in radians
    rot_x += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4x(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "s" && event.keyDown()){
    float theta = rad_step; // The angle of rotation in radians
    rot_x += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4x(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
    /*else if (event.getKeySym() == "Return" && event.keyDown() && !startProcedure){
      initial_guess_vector = sampleRandomPointsOnPlane(planeCloud, initial_guess_number);
      std::cout << "guess_vector_size: " << initial_guess_vector.size() << std::endl;
    }*/
  else{
    if (event.keyDown()){
      std::cout << "Pressed key: " << event.getKeySym() << std::endl;
    }
  }
}

bool checkViewersStop(std::vector<pcl::visualization::PCLVisualizer::Ptr> viewers_vec) {
  for (int i=0; i<viewers_vec.size(); i++){
    if(viewers_vec[i]->wasStopped()) {
      //std::cout << "Viewer #" << i << " has been stopped" << std::endl;
      return false;
    }
  }
  return true;
}

void spinViewers(std::vector<pcl::visualization::PCLVisualizer::Ptr> viewers_vec) {
  for (int i=0; i<viewers_vec.size(); i++)
    viewers_vec[i]->spinOnce(viewer_rate);
}

Eigen::Matrix4f getTransformToInitGuessCenter(pcl::PointXYZ initGuess, pcl::PointCloud<pcl::PointXYZ>::Ptr planeCtr) {
  Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
  transform (0,3) = initGuess.x-planeCtr->points[0].x;
  transform (1,3) = initGuess.y-planeCtr->points[0].y;
  transform (2,3) = initGuess.z-planeCtr->points[0].z;
  return transform;
}

Eigen::Matrix4f getTransformFromObjCenter(pcl::PointXYZ objCloudCenter) {
  Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
  transform (0,3) = -objCloudCenter.x;
  transform (1,3) = -objCloudCenter.y;
  transform (2,3) = -objCloudCenter.z;
  return transform;
}

int getMinIndex(double * vec, int vec_size){
  double min = vec[0];
  int index = 0;
  for (int i=1; i<vec_size; i++) {
    if(vec[i] <= min){
      min = vec[i];
      index = i;
    }
  }
  return index;
}

// --------------
// -----Main-----
// --------------
int main (int argc, char** argv)
{
  srand(time(0));
  if (argc < 5) { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
    printUsage (argv[0]);
    return 0;
  }
  else {
    //reading global params
    if (pcl::console::find_argument (argc, argv, "-h") >= 0) {
      printUsage (argv[0]);
      return 0;
    }
    if (pcl::console::find_argument (argc, argv, "-nd") >= 0) {
      downsample_pointclouds = false;
      cout << "Setting downsampling to false\n";
    }
    if (pcl::console::find_argument (argc, argv, "-v") >= 0) {
      debug_mode = true;
      cout << "Debug mode enabled\n";
    }
    if (pcl::console::parse (argc, argv, "-r", initial_guess_number) >= 0)
      cout << "Setting initial_guess_number to " << initial_guess_number << "\n";
    if (pcl::console::parse (argc, argv, "-md", max_distance) >= 0)
      cout << "Setting max_distance to "<<max_distance<<"\n";
    if (pcl::console::parse (argc, argv, "-mi", max_ICP_iters) >= 0)
      cout << "Setting max_ICP_iters to "<<max_ICP_iters<<"\n";
    if (pcl::console::parse (argc, argv, "-mc", min_chisquare_error) >= 0)
      cout << "Setting min_chisquare_error to "<<min_chisquare_error<<"\n";

    //reading the point clouds
    std::string scene_filename;
    std::string object_filename;
    if (pcl::console::parse (argc, argv, "-s", scene_filename) >= 0 && pcl::console::parse (argc, argv, "-o", object_filename) >= 0){
      cout << "\nReading scene and object point clouds from " << scene_filename << " and " << object_filename << std::endl;
    }
    MyPointCloud sceneCloud;
    MyPointCloud objectCloud;
    if (sceneCloud.loadPointCloud(scene_filename, "scene_cloud", scene_color_r, scene_color_g, scene_color_b, downsample_pointclouds) && objectCloud.loadPointCloud(object_filename, "obj_cloud", obj_color_r, obj_color_g, obj_color_b, downsample_pointclouds))
    {
      std::cout << "\nLoaded " << sceneCloud.cloud()->width * sceneCloud.cloud()->height << " data points from " << sceneCloud.filename() << std::endl;
      std::cout << "Loaded " << objectCloud.cloud()->width * objectCloud.cloud()->height << " data points from " << objectCloud.filename() << std::endl;
    }
    else
    {
      std::cout << "Couldn't load the clouds." << std::endl;
    }

    std::cout << "\nPlease, select the plane where performing the search and then press q to start the procedure:" << std::endl;
    printPlaneSelectionUsage();

    //initialize viewer
    pcl::visualization::PCLVisualizer viewer ("3D Viewer - PointClouds and PlaneSelection");
    viewer.setBackgroundColor(viewer_bground_r, viewer_bground_g, viewer_bground_b);
    //visualize scene rangeImagePointCloud
    viewer.addPointCloud(sceneCloud.cloud(), *sceneCloud.color_handler(), sceneCloud.cloud_id());
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, sceneCloud.cloud_id());
    //visualize object rangeImagePointCloud
    viewer.addPointCloud(objectCloud.cloud(), *objectCloud.color_handler(), objectCloud.cloud_id());
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, objectCloud.cloud_id());
    //set default view on viewer
    viewer.setCameraPosition (-0.017009, 0.903684, -3.40063, -0.0808411, -0.986477, -0.142573);
    viewer.resetCamera();

    // Generate the plane data
    createPlane(planeCloud, planeCenter, plane_w, plane_h, plane_res);

    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> plane_color_handler(planeCloud, plane_color_r, plane_color_g, plane_color_b);
    viewer.addPointCloud(planeCloud, plane_color_handler, "plane_cloud");
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, "plane_cloud");
    viewer.addPointCloud(planeCenter, plane_color_handler, "plane_center");
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_plane_center_point_size, "plane_center");

    //register the keyboard callback
    viewer.registerKeyboardCallback(keyboardEventOccurred, (void*)&viewer);

    while (!viewer.wasStopped())
    {
      viewer.spinOnce(viewer_rate);
      viewer.removePointCloud("plane_cloud");
      viewer.removePointCloud("plane_center");
      viewer.removePointCloud("plane_normals");
      viewer.addPointCloud(planeCloud, plane_color_handler, "plane_cloud");
      viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, "plane_cloud");
      viewer.addPointCloud(planeCenter, plane_color_handler, "plane_center");
      viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_plane_center_point_size, "plane_center");
      if(show_normals) {
        //compute normals
        pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
        ne.setInputCloud(planeCloud);
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
        ne.setSearchMethod(tree);
        pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);
        ne.setRadiusSearch(0.05);
        ne.compute(*cloud_normals);
        viewer.addPointCloudNormals<pcl::PointXYZ, pcl::Normal>(planeCloud, cloud_normals, 50, 0.05, "plane_normals");
      }
    }

    //start the procedure after the first viewer is dead
    //std::cout << "\nStarting the procedure with " << initial_guess_number << " random initial guesses ..." << std::endl;
    //get initial guesses
    initial_guess_vector = sampleRandomPointsOnPlane(planeCloud, initial_guess_number);
    //init the clouds
    pcl::PointXYZ objCloudCenter = objectCloud.getCloudCenterOfMass();
    Eigen::Matrix4f transform_object_to_origin = getTransformFromObjCenter(objCloudCenter);
    Eigen::Matrix4f main_transform_object_plane = transform_to_plane_center *
                                                  R4z(rot_z) *
                                                  R4y(rot_y) *
                                                  R4x(rot_x) *
                                                  transform_object_to_origin;
    std::vector<MyPointCloud> objCloudsTransformed(initial_guess_number);
    //declare the viewers
    std::vector<pcl::visualization::PCLVisualizer::Ptr> viewers_vector;
    //declare the solvers
    std::vector<My3DICPSolver> solvers_vector;
    Eigen::Affine3f init_guess_identity = Eigen::Affine3f::Identity();

    for (int i=0; i<initial_guess_number; i++){
      std::stringstream sstmp;
      sstmp << "3D Viewer - Guess #" << i;
      std::string viewer_name = sstmp.str();
      //transform obj cloud according to intial guesses
      objCloudsTransformed[i].loadPointCloud(objectCloud);
      Eigen::Matrix4f transform_to_init_guess_center = getTransformToInitGuessCenter(initial_guess_vector[i], planeCenter);
      Eigen::Matrix4f temp_transform = transform_to_init_guess_center * main_transform_object_plane;
      objCloudsTransformed[i].transformCloud(temp_transform);
      if(debug_mode) {
        pcl::visualization::PCLVisualizer::Ptr viewer_ptr = boost::shared_ptr<pcl::visualization::PCLVisualizer>(
            new pcl::visualization::PCLVisualizer(viewer_name));
        viewer_ptr->setBackgroundColor(viewer_bground_r, viewer_bground_g, viewer_bground_b);
        //visualize scene Cloud
        viewer_ptr->addPointCloud(sceneCloud.cloud(), *sceneCloud.color_handler(), sceneCloud.cloud_id());
        viewer_ptr->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE,
                                                     rendering_point_size, sceneCloud.cloud_id());
        //visualize object Cloud
        viewer_ptr->addPointCloud(objCloudsTransformed[i].cloud(), *objCloudsTransformed[i].color_handler(),
                                  objCloudsTransformed[i].cloud_id());
        viewer_ptr->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE,
                                                     rendering_point_size, objCloudsTransformed[i].cloud_id());
        //set default view on viewer
        viewer_ptr->setCameraPosition(-0.017009, 0.903684, -3.40063, -0.0808411, -0.986477, -0.142573);
        viewer_ptr->resetCamera();
        //visualize plane
        //viewer_ptr->addPointCloud(planeCloud, plane_color_handler, "plane_cloud");
        //viewer_ptr->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, "plane_cloud");
        //add viewer to viewers_vector
        viewers_vector.push_back(viewer_ptr);
      }
      //declare the solver
      solvers_vector.push_back(My3DICPSolver(objCloudsTransformed[i], sceneCloud, init_guess_identity));
    }

    std::cout << "#### Starting Least Square procedure with " << initial_guess_number << " random initial guesses ..." << std::endl;
    int ICP_iters_ctr_vector[initial_guess_number];
    bool continue_running_ICP_vector[initial_guess_number];
    for(int i=0; i<initial_guess_number; i++) {
      ICP_iters_ctr_vector[i] = 0;
      continue_running_ICP_vector[i] = true;
    }
    double chi_stats_vector[initial_guess_number][max_ICP_iters];

    if(debug_mode) {
      while (checkViewersStop(viewers_vector)) {
        int check_procedure_running = 0;
        for (int i = 0; i < initial_guess_number; i++) {
          if (!continue_running_ICP_vector[i])
            check_procedure_running++;
        }
        if (check_procedure_running == initial_guess_number) {
          //stop the procedure
          break;
        }
        for (int i = 0; i < initial_guess_number; i++) {
          if (ICP_iters_ctr_vector[i] < max_ICP_iters && continue_running_ICP_vector[i]) {
            std::vector<Correspondence> corrs;
            //the cloud of the object is the source_cloud in the correspondence finder!!!
            int corr_num = objCloudsTransformed[i].findCorrespondences(sceneCloud, max_distance, corrs);
            if (corr_num == 0) {
              continue_running_ICP_vector[i] = false;
              std::cout << "Solver(" << i << ") - Least Square terminated after " << ICP_iters_ctr_vector[i] + 1
                        << " iterations - No correspondences found" << std::endl;
            } else {
              //std::cout << "Num corr: " << corr_num << std::endl;
              //draw correspondences
              //viewer.drawCorrespondences(objectCloud.cloud_id(), sceneCloud.cloud_id(), corrs);
              //one round
              solvers_vector[i].setCorrespondences(corrs);
              chi_stats_vector[i][ICP_iters_ctr_vector[i]] = solvers_vector[i].doOneRoundICP();
              //std::cout << "Chi square error solver(" << i << "): " << chi_stats_vector[i][ICP_iters_ctr_vector[i]] << std::endl;
              //Eigen::Affine3f tf = solver.getTransform();
              //objectCloud.transformCloud(tf.matrix());
              objCloudsTransformed[i] = solvers_vector[i].sourceCloud();
              //viewer.updateCloud(objectCloud);
              viewers_vector[i]->removePointCloud(objCloudsTransformed[i].cloud_id());
              viewers_vector[i]->addPointCloud(objCloudsTransformed[i].cloud(),
                                               *objCloudsTransformed[i].color_handler(),
                                               objCloudsTransformed[i].cloud_id());
              viewers_vector[i]->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE,
                                                                  rendering_point_size,
                                                                  objCloudsTransformed[i].cloud_id());
              if (chi_stats_vector[i][ICP_iters_ctr_vector[i]] <= min_chisquare_error) {
                continue_running_ICP_vector[i] = false;
                std::cout << "Solver(" << i << ") - Least Square converged within " << ICP_iters_ctr_vector[i] + 1
                          << " iterations - Final Least Square Error: "
                          << chi_stats_vector[i][ICP_iters_ctr_vector[i]] << std::endl;
                //std::cout << "Solver(" << i << ") - Last transform found:\n" << solvers_vector[i].getTransform().matrix() << std::endl;
//                std::cout << "Solver(" << i << ") - Final Least Square Error: "
//                          << chi_stats_vector[i][ICP_iters_ctr_vector[i]] << std::endl;
              } else {
                ICP_iters_ctr_vector[i]++;
              }
            }
          }
          if (ICP_iters_ctr_vector[i] == max_ICP_iters && continue_running_ICP_vector[i]) {
            continue_running_ICP_vector[i] = false;
            std::cout << "Solver(" << i << ") - Least square terminated - Max iterations reached - Final Least Square Error: "
                      << chi_stats_vector[i][ICP_iters_ctr_vector[i]-1] << std::endl;
//            std::cout << "Solver(" << i << ") - Final Least Square Error: "
//                      << chi_stats_vector[i][ICP_iters_ctr_vector[i]-1] << std::endl;
          }
          viewers_vector[i]->spinOnce(viewer_rate);
        }
        //spinViewers(viewers_vector);
      }
    }
    else {
      while (true) {
        int check_procedure_running = 0;
        for (int i = 0; i < initial_guess_number; i++) {
          if (!continue_running_ICP_vector[i])
            check_procedure_running++;
        }
        if (check_procedure_running == initial_guess_number) {
          //stop the procedure
          break;
        }
        for (int i = 0; i < initial_guess_number; i++) {
          if (ICP_iters_ctr_vector[i] < max_ICP_iters && continue_running_ICP_vector[i]) {
            std::vector<Correspondence> corrs;
            //the cloud of the object is the source_cloud in the correspondence finder!!!
            int corr_num = objCloudsTransformed[i].findCorrespondences(sceneCloud, max_distance, corrs);
            if (corr_num == 0) {
              continue_running_ICP_vector[i] = false;
              std::cout << "Solver(" << i << ") - Least Square terminated after " << ICP_iters_ctr_vector[i] + 1
                        << " iterations - No correspondences found" << std::endl;
            } else {
              //std::cout << "Num corr: " << corr_num << std::endl;
              //draw correspondences
              //viewer.drawCorrespondences(objectCloud.cloud_id(), sceneCloud.cloud_id(), corrs);
              //one round
              solvers_vector[i].setCorrespondences(corrs);
              chi_stats_vector[i][ICP_iters_ctr_vector[i]] = solvers_vector[i].doOneRoundICP();
              //std::cout << "Chi square error solver(" << i << "): " << chi_stats_vector[i][ICP_iters_ctr_vector[i]] << std::endl;
              //Eigen::Affine3f tf = solver.getTransform();
              //objectCloud.transformCloud(tf.matrix());
              objCloudsTransformed[i] = solvers_vector[i].sourceCloud();
              if (chi_stats_vector[i][ICP_iters_ctr_vector[i]] <= min_chisquare_error) {
                continue_running_ICP_vector[i] = false;
                std::cout << "Solver(" << i << ") - Least Square converged within " << ICP_iters_ctr_vector[i] + 1
                          << " iterations - Final Least Square Error: "
                          << chi_stats_vector[i][ICP_iters_ctr_vector[i]] << std::endl;
                //std::cout << "Solver(" << i << ") - Last transform found:\n" << solvers_vector[i].getTransform().matrix() << std::endl;
//                std::cout << "Solver(" << i << ") - Final Least Square Error: "
//                          << chi_stats_vector[i][ICP_iters_ctr_vector[i]] << std::endl;
              } else {
                ICP_iters_ctr_vector[i]++;
              }
            }
          }
          if (ICP_iters_ctr_vector[i] == max_ICP_iters && continue_running_ICP_vector[i]) {
            continue_running_ICP_vector[i] = false;
            std::cout << "Solver(" << i << ") - Least square terminated - Max iterations reached - Final Least Square Error: "
                      << chi_stats_vector[i][ICP_iters_ctr_vector[i]-1] << std::endl;
//            std::cout << "Solver(" << i << ") - Final Least Square Error: "
//                      << chi_stats_vector[i][ICP_iters_ctr_vector[i]-1] << std::endl;
          }
        }
      }
    }

    std::cout << "#### Least Square procedure terminated" << std::endl << std::endl;

    //compute the overall error for each of the initial guesses
    double overall_errors[initial_guess_number];
    for(int i=0; i<initial_guess_number; i++) {
      overall_errors[i] = objCloudsTransformed[i].computeOverallErrorFromCloud(sceneCloud);
      if (debug_mode)
        std::cout << "Overall error initial guess " << i << ": " << overall_errors[i] << std::endl;
    }
    if (debug_mode)
      std::cout << "The object should be the one detected starting from " << getMinIndex(overall_errors, initial_guess_number) << " initial guess" << std::endl;
//    //just wait for quitting
//    while (checkViewersStop(viewers_vector)) {
//      spinViewers(viewers_vector);
//    }

    //open new viewer with bounding box around possible object
    int final_point_size = 5;
    pcl::visualization::PCLVisualizer last_viewer ("3D Viewer - Object within the scene");
    //visualize scene rangeImagePointCloud
    last_viewer.addPointCloud(sceneCloud.cloud(), *sceneCloud.color_handler(), sceneCloud.cloud_id());
    last_viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, final_point_size, sceneCloud.cloud_id());
    //visualize object cloud transformed
    //set color_handler
    int best_guess_id = getMinIndex(overall_errors, initial_guess_number);
    int r=255, g=0, b=0;
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>::Ptr obj_guess_color_handler =
        boost::shared_ptr<pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> >(
            new pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>(objCloudsTransformed[best_guess_id].cloud(), r, g, b)
        );
    last_viewer.setCameraPosition(-0.017009, 0.903684, -3.40063, -0.0808411, -0.986477, -0.142573);
    last_viewer.resetCamera();
    last_viewer.addPointCloud(objCloudsTransformed[best_guess_id].cloud(),
                              *obj_guess_color_handler,
                              objCloudsTransformed[best_guess_id].cloud_id());
    last_viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE,
                                                 final_point_size,
                                                 objCloudsTransformed[best_guess_id].cloud_id());
//    //show bounding box on the scene point cloud
//    pcl::PointXYZ obj_min_point, obj_max_point;
//    pcl::getMinMax3D(*objCloudsTransformed[getMinIndex(overall_errors, initial_guess_number)].cloud(), obj_min_point, obj_max_point);
//    last_viewer.addCube(obj_min_point.x, obj_max_point.x,
//                        obj_min_point.y, obj_max_point.y,
//                        obj_min_point.z, obj_max_point.z,
//                        255, 0, 0,
//                        "bbox");
    //just wait for quitting
    std::cout << "Press q for exit ..." << std::endl;
    while (!last_viewer.wasStopped()) {
      last_viewer.spinOnce(viewer_rate);
    }

    return (0);

  }
}