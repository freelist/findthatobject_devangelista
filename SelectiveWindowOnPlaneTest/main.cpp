#include "MyPointCloud.h"
#include "MyGeometryFunctions.h"

// --------------------------
// -----Global Variables-----
// --------------------------
// (these can be given in input)
bool downsample_pointclouds = true;
int initial_guess_number = 10;
// (these cannot be given in input)
// viewer params
int viewer_rate = 100;
int rendering_point_size = 1;
int rendering_plane_center_point_size = 7;
int viewer_bground_r = 0; int viewer_bground_g = 0; int viewer_bground_b = 0;
int scene_color_r = 0; int scene_color_g = 0; int scene_color_b = 255;
int obj_color_r = 0; int obj_color_g = 255; int obj_color_b = 0;
int plane_color_r = 255; int plane_color_g = 0; int plane_color_b = 0;
int plane_w = 1;
int plane_h = 1;
float plane_res = 0.01;
bool show_normals = false;

//plane data pointers and vars
pcl::PointCloud<pcl::PointXYZ>::Ptr planeCloud(new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr planeCenter(new pcl::PointCloud<pcl::PointXYZ>);
Eigen::Matrix4f transform_from_plane_center = Eigen::Matrix4f::Identity();
Eigen::Matrix4f transform_to_plane_center = Eigen::Matrix4f::Identity();
float rot_x = 0.0;
float rot_y = 0.0;
float rot_z = 0.0;
std::vector<pcl::PointXYZ> initial_guess_vector;
bool startProcedure = false;

// --------------
// -----Help-----
// --------------
std::string ExtractFilename( const std::string& path )
{
  return path.substr( path.find_last_of( '/' ) +1 );
}
void printUsage (const char* progPath)
{
  std::string progName = ExtractFilename(std::string(progPath));
  std::cout << "\n\nUsage: ./"<<progName<<" -s <scene.pcd> -o <object.pcd> [Options]\n"
            << "Example: ./"<<progName<<" -s scene.pcd -o object.pcd -r 4\n\n"
            << "Options:\n"
            << "-------------------------------------------\n"
            << "-s <string>   path of the scene.pcd file\n"
            << "-o <string>   path of the object.pcd file\n"
            << "-r <int>      number of random initial guess on plane (default is 10)\n"
            << "-nd           if given, downsampling of the point clouds is disabled\n"
            << "-h            this help\n"
            << "\n\n";
}

//keyboardEvent Callback
void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event, void* viewer_void)
{
  float step = 0.05;
  float rad_step = 0.1;
  transform_from_plane_center.setIdentity();
  transform_from_plane_center (0,3) = -planeCenter->points[0].x;
  transform_from_plane_center (1,3) = -planeCenter->points[0].y;
  transform_from_plane_center (2,3) = -planeCenter->points[0].z;
  transform_to_plane_center.setIdentity();
  transform_to_plane_center (0,3) = planeCenter->points[0].x;
  transform_to_plane_center (1,3) = planeCenter->points[0].y;
  transform_to_plane_center (2,3) = planeCenter->points[0].z;
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *>(viewer_void);
  if (event.getKeySym() == "n" && event.keyDown()) {
    show_normals = !show_normals;
  }
  else if (event.getKeySym() == "Up" && event.keyDown()){
    Eigen::Matrix4f transform_up = Eigen::Matrix4f::Identity();
    transform_up (1,3) = -step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_up);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "Down" && event.keyDown()){
    Eigen::Matrix4f transform_down = Eigen::Matrix4f::Identity();
    transform_down (1,3) = step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_down);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "Left" && event.keyDown()){
    Eigen::Matrix4f transform_left = Eigen::Matrix4f::Identity();
    transform_left (0,3) = -step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_left);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "Right" && event.keyDown()){
    Eigen::Matrix4f transform_right = Eigen::Matrix4f::Identity();
    transform_right (0,3) = step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_right);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "egrave" && event.keyDown()){
    Eigen::Matrix4f transform_zoom_in = Eigen::Matrix4f::Identity();
    transform_zoom_in (2,3) = step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_zoom_in);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "agrave" && event.keyDown()){
    Eigen::Matrix4f transform_zoom_out = Eigen::Matrix4f::Identity();
    transform_zoom_out (2,3) = -step;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_zoom_out);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "ograve" && event.keyDown()){
    float theta = -rad_step; // The angle of rotation in radians
    rot_z += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4z(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "ugrave" && event.keyDown()){
    float theta = rad_step; // The angle of rotation in radians
    rot_z += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4z(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "a" && event.keyDown()){
    float theta = rad_step; // The angle of rotation in radians
    rot_y += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4y(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "d" && event.keyDown()){
    float theta = -rad_step; // The angle of rotation in radians
    rot_y += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4y(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "w" && event.keyDown()){
    float theta = -rad_step; // The angle of rotation in radians
    rot_x += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4x(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
  else if (event.getKeySym() == "s" && event.keyDown()){
    float theta = rad_step; // The angle of rotation in radians
    rot_x += theta;
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_from_plane_center);
    pcl::transformPointCloud(*planeCloud, *planeCloud, R4x(theta));
    pcl::transformPointCloud(*planeCloud, *planeCloud, transform_to_plane_center);
    computePlaneCenter(planeCloud, planeCenter);
  }
    /*else if (event.getKeySym() == "Return" && event.keyDown() && !startProcedure){
      initial_guess_vector = sampleRandomPointsOnPlane(planeCloud, initial_guess_number);
      std::cout << "guess_vector_size: " << initial_guess_vector.size() << std::endl;
    }*/
  else{
    if (event.keyDown()){
      std::cout << "Pressed key: " << event.getKeySym() << std::endl;
    }
  }
}

bool checkViewersStop(std::vector<pcl::visualization::PCLVisualizer::Ptr> viewers_vec) {
  for (int i=0; i<viewers_vec.size(); i++){
    if(viewers_vec[i]->wasStopped()) {
      //std::cout << "Viewer #" << i << " has been stopped" << std::endl;
      return false;
    }
  }
  return true;
}

void spinViewers(std::vector<pcl::visualization::PCLVisualizer::Ptr> viewers_vec) {
  for (int i=0; i<viewers_vec.size(); i++)
    viewers_vec[i]->spinOnce(viewer_rate);
}

Eigen::Matrix4f getTransformToInitGuessCenter(pcl::PointXYZ initGuess, pcl::PointCloud<pcl::PointXYZ>::Ptr planeCtr) {
  Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
  transform (0,3) = initGuess.x-planeCtr->points[0].x;
  transform (1,3) = initGuess.y-planeCtr->points[0].y;
  transform (2,3) = initGuess.z-planeCtr->points[0].z;
  return transform;
}

Eigen::Matrix4f getTransformFromObjCenter(pcl::PointXYZ objCloudCenter) {
  Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
  transform (0,3) = -objCloudCenter.x;
  transform (1,3) = -objCloudCenter.y;
  transform (2,3) = -objCloudCenter.z;
  return transform;
}

// --------------
// -----Main-----
// --------------
int main (int argc, char** argv)
{
  if (argc < 5) { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
    printUsage (argv[0]);
    return 0;
  }
  else {
    //reading global params
    if (pcl::console::find_argument (argc, argv, "-h") >= 0) {
      printUsage (argv[0]);
      return 0;
    }
    if (pcl::console::find_argument (argc, argv, "-nd") >= 0) {
      downsample_pointclouds = false;
      cout << "Setting downsampling to false\n";
    }
    if (pcl::console::parse (argc, argv, "-r", initial_guess_number) >= 0) {
      cout << "Setting initial_guess_number to " << initial_guess_number << "\n";
    }

    //reading the point clouds
    std::string scene_filename;
    std::string object_filename;
    if (pcl::console::parse (argc, argv, "-s", scene_filename) >= 0 && pcl::console::parse (argc, argv, "-o", object_filename) >= 0){
      cout << "\nReading scene and object point clouds from " << scene_filename << " and " << object_filename << std::endl;
    }
    MyPointCloud sceneCloud;
    MyPointCloud objectCloud;
    if (sceneCloud.loadPointCloud(scene_filename, "scene_cloud", scene_color_r, scene_color_g, scene_color_b, downsample_pointclouds) && objectCloud.loadPointCloud(object_filename, "obj_cloud", obj_color_r, obj_color_g, obj_color_b, downsample_pointclouds))
    {
      std::cout << "\nLoaded " << sceneCloud.cloud()->width * sceneCloud.cloud()->height << " data points from " << sceneCloud.filename() << std::endl;
      std::cout << "Loaded " << objectCloud.cloud()->width * objectCloud.cloud()->height << " data points from " << objectCloud.filename() << std::endl;
    }
    else
    {
      std::cout << "Couldn't load the clouds." << std::endl;
    }

    //initialize viewer
    pcl::visualization::PCLVisualizer viewer ("3D Viewer - PointClouds and PlaneSelection");
    viewer.setBackgroundColor(viewer_bground_r, viewer_bground_g, viewer_bground_b);
    //visualize scene rangeImagePointCloud
    viewer.addPointCloud(sceneCloud.cloud(), *sceneCloud.color_handler(), sceneCloud.cloud_id());
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, sceneCloud.cloud_id());
    //visualize object rangeImagePointCloud
    viewer.addPointCloud(objectCloud.cloud(), *objectCloud.color_handler(), objectCloud.cloud_id());
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, objectCloud.cloud_id());

    // Generate the plane data
    createPlane(planeCloud, planeCenter, plane_w, plane_h, plane_res);

    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> plane_color_handler(planeCloud, plane_color_r, plane_color_g, plane_color_b);
    viewer.addPointCloud(planeCloud, plane_color_handler, "plane_cloud");
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, "plane_cloud");
    viewer.addPointCloud(planeCenter, plane_color_handler, "plane_center");
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_plane_center_point_size, "plane_center");

    //register the keyboard callback
    viewer.registerKeyboardCallback(keyboardEventOccurred, (void*)&viewer);

    while (!viewer.wasStopped())
    {
      viewer.spinOnce(viewer_rate);
      viewer.removePointCloud("plane_cloud");
      viewer.removePointCloud("plane_center");
      viewer.removePointCloud("plane_normals");
      viewer.addPointCloud(planeCloud, plane_color_handler, "plane_cloud");
      viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, "plane_cloud");
      viewer.addPointCloud(planeCenter, plane_color_handler, "plane_center");
      viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_plane_center_point_size, "plane_center");
      if(show_normals) {
        //compute normals
        pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
        ne.setInputCloud(planeCloud);
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
        ne.setSearchMethod(tree);
        pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);
        ne.setRadiusSearch(0.05);
        ne.compute(*cloud_normals);
        viewer.addPointCloudNormals<pcl::PointXYZ, pcl::Normal>(planeCloud, cloud_normals, 50, 0.05, "plane_normals");
      }
    }

    //start the procedure after the first viewer is dead
    std::cout << "\nStarting the procedure with " << initial_guess_number << " random initial guesses ..." << std::endl;
    //get initial guesses
    initial_guess_vector = sampleRandomPointsOnPlane(planeCloud, initial_guess_number);
    //init the clouds
    pcl::PointXYZ objCloudCenter = objectCloud.getCloudCenterOfMass();
    Eigen::Matrix4f transform_object_to_origin = getTransformFromObjCenter(objCloudCenter);
    Eigen::Matrix4f main_transform_object_plane = transform_to_plane_center *
                                                  R4z(rot_z) *
                                                  R4y(rot_y) *
                                                  R4x(rot_x) *
                                                  transform_object_to_origin;
    std::vector<MyPointCloud> objCloudsTransformed(initial_guess_number);
    //declare the viewers
    std::vector<pcl::visualization::PCLVisualizer::Ptr> viewers_vector;


    for (int i=0; i<initial_guess_number; i++){
      std::stringstream sstmp;
      sstmp << "3D Viewer - Guess #" << i;
      std::string viewer_name = sstmp.str();
      //transform obj cloud according to intial guesses
      objCloudsTransformed[i].loadPointCloud(objectCloud);
      Eigen::Matrix4f transform_to_init_guess_center = getTransformToInitGuessCenter(initial_guess_vector[i], planeCenter);
      Eigen::Matrix4f temp_transform = transform_to_init_guess_center * main_transform_object_plane;
      objCloudsTransformed[i].transformCloud(temp_transform);
      pcl::visualization::PCLVisualizer::Ptr viewer_ptr = boost::shared_ptr<pcl::visualization::PCLVisualizer >( new pcl::visualization::PCLVisualizer(viewer_name) );
      viewer_ptr->setBackgroundColor(viewer_bground_r, viewer_bground_g, viewer_bground_b);
      //visualize scene rangeImagePointCloud
      viewer_ptr->addPointCloud(sceneCloud.cloud(), *sceneCloud.color_handler(), sceneCloud.cloud_id());
      viewer_ptr->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, sceneCloud.cloud_id());
      //visualize object rangeImagePointCloud
      viewer_ptr->addPointCloud(objCloudsTransformed[i].cloud(), *objCloudsTransformed[i].color_handler(), objCloudsTransformed[i].cloud_id());
      viewer_ptr->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, objCloudsTransformed[i].cloud_id());
      viewers_vector.push_back(viewer_ptr);
      //visualize plane
      viewer_ptr->addPointCloud(planeCloud, plane_color_handler, "plane_cloud");
      viewer_ptr->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, rendering_point_size, "plane_cloud");
    }

    while (checkViewersStop(viewers_vector)) {
      spinViewers(viewers_vector);
    }

    return (0);

  }
}