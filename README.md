# README #

This repository contains the main software developed by Daniele Evangelista for the project "Find That Object" for the exam of Probabilistic Robotics at La Sapienza University of Rome. The goal of the project is to detect a 3D object given in input as a point cloud within a 3D scene point cloud.

### What does this repository contains? ###

* data - the folder where all the data used for the examples are stored
* mylibs - the folder where all the libraries are stored (.h and .cpp files) 
* Report - the folder where the latex project of the report is stored
* LeastSquareTest - the code for the simple Least Square Test
* SelectiveWindowOnPlaneTest - the code for the selection of the plane where to initialize the initial guesses
* RandomInitialGuessTest - the code where the complete project has been developed

### How to BUILD all the examples ###

* cd <name_of_the_code_folder>
* mkdir build-release
* cd build-release
* cmake ..
* make -j [as much as u can]

### How to RUN all the examples ###

* cd <name_of_the_code_folder>
* cd build-release
* ./<name_of_the_bin> -h [this will show you the usage helper of each example]
* follow the examples =)
